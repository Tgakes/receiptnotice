package com.easy.utils.base;

import java.io.File;

public class FileConstant {
    /**
     * 文件根路径
     */
    public static final String BASE_FILE_PATH = "videoCapture" + File.separator;


    /**
     * 版本更新APK
     */
    public static final int TYPE_APP_UPDATE = -1;
    /**
     * 文本
     */
    public static final int TYPE_TEXT = 0;
    /**
     * 图片
     */
    public static final int TYPE_PHOTO = 1;

    /**
     * 音频--音乐
     */
    public static final int TYPE_AUDIO = 2;

    /**
     * 视频
     */
    public static final int TYPE_VIDEO = 3;
    /**
     * apk
     */
    public static final int TYPE_APP = 4;
    /**
     * 其他
     */
    public static final int TYPE_OTHER = 5;
    /**
     * 文本保存路径
     */
    public static final String SAVE_TEXT_PATH = BASE_FILE_PATH + "text" + File.separator;
    /**
     * 应用文件保存路径
     */
    public static final String SAVE_APP_PATH = BASE_FILE_PATH + "app" + File.separator;
    /**
     * 音频--音乐位置
     */
    public static final String SAVE_AUDIO_PATH = BASE_FILE_PATH + "audio" + File.separator;
    /**
     * 视频位置
     */
    public static final String SAVE_VIDEO_PATH = BASE_FILE_PATH + "video" + File.separator;
    /**
     * 图片保存路径
     */
    public static final String SAVE_PHOTO_PATH = BASE_FILE_PATH + "photo" + File.separator;
    /**
     * 其他
     */
    public static final String SAVE_OTHER_PATH = BASE_FILE_PATH + "other" + File.separator;
}
