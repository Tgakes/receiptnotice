package com.easy.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.easy.utils.bean.ModelSlotAndSub;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimUtils {


    //判断是否是双卡
    public static boolean isDoubleSim(Context mContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager manager = SubscriptionManager.from(mContext);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
            int count = manager.getActiveSubscriptionInfoCount();
            if (count == 2) {
                return true;
            }
        }
        return false;
    }

    //通过卡槽id获取sim卡的信息0代码卡槽1，1代表卡槽2
    public static String getSlotIdInfo(Context mContext, int slotId) {
        String info = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager manager = SubscriptionManager.from(mContext);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            SubscriptionInfo subInfo = manager.getActiveSubscriptionInfoForSimSlotIndex(slotId);
            if (subInfo != null) {
                info = subInfo.getCarrierName().toString();
            }
        }
        return info;
    }

    //判断手机中是否装了双卡
    public static boolean isHasDoubleSim(Context mContext) {
        try {
            if (isDoubleSim(mContext)) {
                List<ModelSlotAndSub> slotAndSubs = getModelSlot(mContext);
                if (slotAndSubs != null && slotAndSubs.size() == 2) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    //判断是否拥有该权限
    public static boolean IsHasPermission(Context mContext, String permission) {
        if (mContext != null) {
            if (ActivityCompat.checkSelfPermission(mContext, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }


    //申请该权限
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void requestOnePermission(Activity activity, String permission, int permissionCode) {
        if (activity != null) {
            activity.requestPermissions(new String[]{permission}, permissionCode);
        }
    }


    //获取icc用来判断
    public static int getIcc(String iccid, List<ModelSlotAndSub> slotAndSubs) {
        int icc = 0;
        if (slotAndSubs != null && slotAndSubs.size() >= 2 && !TextUtils.isEmpty(iccid)) {
            if (iccid.startsWith(slotAndSubs.get(0).iccid)) {
                icc = 1;
            } else if (iccid.startsWith(slotAndSubs.get(1).iccid)) {
                icc = 2;
            } else {
                if (iccid.equals(slotAndSubs.get(0).subId)) {
                    icc = 1;
                } else if (iccid.equals(slotAndSubs.get(1).subId)) {
                    icc = 2;
                } else {
                    icc = 0;
                }
            }
        }
        return icc;
    }

    /**
     * 获取来自那张卡 即卡1 还是卡2
     *
     * @param mContext
     * @param subID
     * @return
     */
    public static int getSlot(Context mContext, String subID) {
        return getIcc(subID, getModelSlot(mContext));
    }

    //获取对应的卡槽ID和iccID 关联
    private static List<ModelSlotAndSub> getModelSlot(Context mContext) {
        List<ModelSlotAndSub> data = new ArrayList<>();
        ModelSlotAndSub modelSlotAndSub = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                List<SubscriptionInfo> mSubcriptionInfos = subscriptionManager.getActiveSubscriptionInfoList();
                if (mSubcriptionInfos != null) {
                    int size = mSubcriptionInfos.size();
                    for (int i = 0; i < size; i++) {
                        SubscriptionInfo info = mSubcriptionInfos.get(i);
                        if (info != null) {
                            modelSlotAndSub = new ModelSlotAndSub();
                            modelSlotAndSub.subId = info.getSubscriptionId() + "";
                            modelSlotAndSub.simSlot = info.getSimSlotIndex() + 1 + "";
                            modelSlotAndSub.iccid = info.getIccId();
                            data.add(modelSlotAndSub);
                            //id=1, iccId=898601178[****] simSlotIndex=0
                            //id=2, iccId=898603189[****] simSlotIndex=1

                            //{id=1, iccId=898601178[****] simSlotIndex=0
                            //{id=3, iccId=898600401[****] simSlotIndex=1
                        }
                    }
                    //qb2019/07/02修改将卡1和卡2的位置旋转过来，如果第一个simSlot大于第二个的话
                    if (data != null && data.size() == 2) {
                        if (!TextUtils.isEmpty(data.get(0).simSlot) && !TextUtils.isEmpty(data.get(1).simSlot)) {
                            int simSlot1 = Integer.valueOf(data.get(0).simSlot);
                            int simSlot2 = Integer.valueOf(data.get(1).simSlot);
                            if (simSlot1 > simSlot2) {
                                ModelSlotAndSub modelSlotAndSub1 = data.get(1);
                                data.remove(data.get(1));
                                data.add(0, modelSlotAndSub1);
                            }
                        }
                    }
                }
            }
        }
        return data;
    }

    public static JSONArray getAllSimInfo(Context context) {

        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Class<?> clazz = tel.getClass();

        //获取可以进行反射的字段
        List<EMethod> list = new ArrayList<>();
        Map<String, Integer> listIgnore = new HashMap<>();

        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            String name = method.getName();
            if (!name.startsWith("get"))
                continue;

            if (listIgnore.get(name) != null)
                continue;
            listIgnore.put(name, 0);

            Method m1 = null;
            Method m2 = null;
            Method m3 = null;
            try {
                m1 = clazz.getDeclaredMethod(name);
            } catch (Exception e) {
            }
            try {
                m2 = clazz.getDeclaredMethod(name, int.class);
            } catch (Exception e) {
            }
            try {
                m3 = clazz.getDeclaredMethod(name, long.class);
            } catch (Exception e) {
            }

            if (m1 != null && ((m2 == null && m3 != null) || (m2 != null && m3 == null))) {
                Class<?> c1 = m1.getReturnType();
                Class<?> c2 = m2 == null ? null : m2.getReturnType();
                Class<?> c3 = m3 == null ? null : m3.getReturnType();
                if (m2 == null) {
                    if (c1 == null || c1 != c3)
                        continue;
                } else {
                    if (c1 == null || c1 != c2)
                        continue;
                }
                EMethod item = new EMethod(name, m2 == null ? 1 : 0, c1);
                list.add(item);
            }
        }
        listIgnore.clear();

        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < 10; i++) {
                JSONObject json = new JSONObject();
                for (EMethod em : list) {
                    Method method = null;
                    Object param = null;
                    if (em.type == 0) {
                        method = clazz.getDeclaredMethod(em.name, int.class);
                        param = i;
                    } else {
                        method = clazz.getDeclaredMethod(em.name, long.class);
                        param = new Long(i);
                    }
                    if (!method.isAccessible())
                        method.setAccessible(true);

                    String name = em.name.substring(3);
                    Object value = null;
                    try {
                        value = method.invoke(tel, param);
                    } catch (Exception e) {
                        //前面已经对private设置了可访问，有些还是会报错，就不管这个了
                        continue;
                    }

                    json.put(name, value);
                }

                if (json.optInt("SimState") == TelephonyManager.SIM_STATE_UNKNOWN || json.optInt("SimState") == TelephonyManager.SIM_STATE_ABSENT)
                    continue;

                String imsi = json.optString("SubscriberId");
//                if (imsi == null || imsi.length() == 0)
//                    continue;

                if (EmptyUtils.isNotEmpty(imsi)) {
                    //根据imsi去重
                    boolean repeact = false;
                    int size = array.length();
                    for (int j = 0; j < size; j++) {
                        if (imsi.equals(array.optJSONObject(j).optString("SubscriberId"))) {
                            repeact = true;
                            break;
                        }
                    }
                    if (repeact)
                        continue;
                }


                array.put(json);
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    static class EMethod {
        public String name;
        public int type;    //0为int，1为long
        public Class<?> returnType;    //返回类型

        public EMethod(String name, int type, Class<?> returnType) {
            this.name = name;
            this.type = type;
            this.returnType = returnType;
        }
    }

    /**
     *
     * [
     *     {
     *         "NetworkTypeName":"UNKNOWN",
     *         "PhoneId":0,
     *         "PhoneType":1,
     *         "PhoneTypeFromNetworkType":1,
     *         "PhoneTypeFromProperty":1,
     *         "SubId":0,
     *         "CallState":0,
     *         "CdmaEriIconIndex":-1,
     *         "CdmaEriIconMode":-1,
     *         "CdmaEriText":"GSM nw, no ERI",
     *         "CurrentPhoneType":1,
     *         "DataNetworkType":13,
     *         "DeviceId":"869377032908354",
     *         "DeviceSoftwareVersion":"8693770329083576",
     *         "EmergencyCallbackMode":false,
     *         "GroupIdLevel1":"ffffffff",
     *         "Imei":"869377032908354",
     *         "Line1AlphaTag":"",
     *         "Line1Number":"",
     *         "LteOnCdmaMode":0,
     *         "Meid":"A000008E9D208E",
     *         "Msisdn":"",
     *         "NetworkCountryIso":"cn",
     *         "NetworkOperator":"46000",
     *         "NetworkOperatorName":"中国移动",
     *         "NetworkType":13,
     *         "SimCountryIso":"cn",
     *         "SimOperator":"46000",
     *         "SimOperatorName":"CMCC",
     *         "SimOperatorNumeric":"46000",
     *         "SimSerialNumber":"898600831915854xxxxx",
     *         "SimState":5,
     *         "SubscriberId":"4600007826xxxxx",
     *         "VoiceMailAlphaTag":"语音信箱",
     *         "VoiceMessageCount":0,
     *         "VoiceNetworkType":13
     *     },
     *     {
     *         "NetworkTypeName":"GPRS",
     *         "PhoneId":1,
     *         "PhoneType":1,
     *         "PhoneTypeFromNetworkType":0,
     *         "PhoneTypeFromProperty":2,
     *         "SubId":1,
     *         "CallState":0,
     *         "CdmaEriIconIndex":1,
     *         "CdmaEriIconMode":0,
     *         "CdmaEriText":"漫游指示符正在闪烁",
     *         "CurrentPhoneType":2,
     *         "DataNetworkType":13,
     *         "DeviceId":"A000008E9D208E",
     *         "DeviceSoftwareVersion":"8693770329822076",
     *         "EmergencyCallbackMode":false,
     *         "GroupIdLevel1":"ffffffff",
     *         "Imei":"869377032982201",
     *         "Line1Number":"",
     *         "LteOnCdmaMode":0,
     *         "Meid":"A000008E9D208E",
     *         "Msisdn":"",
     *         "Nai":"460031250956445@mycdma.cn",
     *         "NetworkCountryIso":"cn",
     *         "NetworkOperator":"46011",
     *         "NetworkOperatorName":"中国电信",
     *         "NetworkType":13,
     *         "SimCountryIso":"cn",
     *         "SimOperator":"46011",
     *         "SimOperatorName":"中国电信",
     *         "SimOperatorNumeric":"46011",
     *         "SimSerialNumber":"898603189502004xxxxx",
     *         "SimState":5,
     *         "SubscriberId":"4600312509xxxxx",
     *         "VoiceMailAlphaTag":"语音信箱",
     *         "VoiceMailNumber":"*86",
     *         "VoiceMessageCount":0,
     *         "VoiceNetworkType":7
     *     }
     * ]
     */


}