package com.easy.net.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Response<T> implements Serializable {

    public static String SUCCESS_CODE = "00";
    public static String ERROR_CODE = "no";

    public static String SUCCESS_CODE_NUM = "0";

    public static String SUCCESS_STATUS = "00";//登录成功
    public static String ERROR_STATUS = "01";//登录失败
    public static String ACCOUNT_FREEZING_STATUS = "02";//账号冻结
    public static String NEED_MSG_STATUS = "03";//需要短信验证码

    /**
     * 状态码
     */
    @SerializedName("code")
    private String code;



    /**
     * 描述信息
     */
    @SerializedName("msg")
    private String msg;


    @SerializedName("result")
    String result;

    String addtime;

    String orderid;


    /**
     * 原数据
     */
    private String originalData;

    public String getOriginalData() {
        return originalData;
    }

    public void setOriginalData(String originalData) {
        this.originalData = originalData;
    }


    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    /**
     * 结果返回的数据转化后的对象
     */
    private T resultObj;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public T getResultObj() {
        return resultObj;
    }

    public void setResultObj(T resultObj) {
        this.resultObj = resultObj;
    }

    /**
     * 是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return SUCCESS_CODE.equals(code);
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public String toString() {
        return "Response{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", originalData='" + originalData + '\'' +
                ", resultObj=" + resultObj +
                '}';
    }
}
