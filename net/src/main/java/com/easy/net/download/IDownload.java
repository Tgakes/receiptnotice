package com.easy.net.download;

import com.easy.store.bean.DownloadDo;

public interface IDownload {
    void insertOrUpdate(DownloadDo downloadInfo);

    void delete(DownloadDo downloadInfo);
}
