package com.easy.net.function;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.utils.BinaryStringConvertUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.RsaUtils;

import java.lang.reflect.Type;
import java.net.URLEncoder;

import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

public class DataSwitchFunction<T> implements Function<ResponseBody, Response<T>> {

    Class<T> tClass;

    public DataSwitchFunction(Class<T> t) {
        this.tClass = t;
    }

    @Override
    public Response<T> apply(ResponseBody responseBody) {
        Response response;
        String result = null;
        try {
            result = RsaUtils.decryptRSA(BinaryStringConvertUtils.toString(responseBody.string()));
            Log.i("logs", "=====result====" + result);
            response = JSON.parseObject(result, Response.class);
            response.setOriginalData(result);
            response.setResultObj(onConvert(response));
        } catch (Exception e) {
            response = new Response<>();
            response.setOriginalData(result);
            response.setCode(Response.ERROR_CODE);
            response.setMsg(e.getMessage());
        }
        return response;
    }

    public T onConvert(Response response) {
        if (tClass == String.class) {
            return (T) response.getOriginalData();
        } else
            return JSON.parseObject( response.getResult(), (Type) tClass);
    }
}

