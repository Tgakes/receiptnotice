package com.easy.framework.bean;

public class AppVersion {
    private String new_version;//版本号
    private String url;//下载地址
    private String content;//弹窗提示语

    public String getNew_version() {
        return new_version;
    }

    public void setNew_version(String new_version) {
        this.new_version = new_version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
