package com.easy.framework.dialog;

import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;


public interface WebDownloadDialogView extends BaseView {
    void permissionCallback(Boolean granted,Throwable e);

    void requestFileSize(Response<String> response);
}
