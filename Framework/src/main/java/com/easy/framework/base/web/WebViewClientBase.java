package com.easy.framework.base.web;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.easy.framework.base.WebBaseFragment;

import java.lang.ref.WeakReference;

public class WebViewClientBase extends WebViewClient {

    WeakReference<WebBaseFragment> webViewFragment;

    public WebViewClientBase(WebBaseFragment webViewFragment) {
        this.webViewFragment = new WeakReference<>(webViewFragment);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.d("OverrideUrlLoading", url);
        if (!support(url)) {
            return true;
        }
        return super.shouldOverrideUrlLoading(view, url);
    }

    public boolean support(String url) {
        String newUrl = url.toLowerCase();
        if (newUrl.startsWith("http://") || newUrl.startsWith("https://")) {
            return true;
        }
        return false;
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        request.getRequestHeaders().put("X-Modified-Intercept", "true");
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d("WebViewClientBase", "onPageStarted: " + url);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        Log.d("WebViewClientBase", "onPageFinished: " + url);
        super.onPageFinished(view, url);
        if (webViewFragment.get() != null) {
            webViewFragment.get().onPageFinished(view, url);
        }
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.d("WebViewClientBase", "onReceivedError: " + failingUrl);
        super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
        Log.d("WebViewClientBase", "doUpdateVisitedHistory: " + url);
        super.doUpdateVisitedHistory(view, url, isReload);
        if (webViewFragment.get() != null) {
            webViewFragment.get().doUpdateVisitedHistory(url);
        }
    }
}
