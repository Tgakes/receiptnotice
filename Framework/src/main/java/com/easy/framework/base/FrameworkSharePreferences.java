package com.easy.framework.base;


import androidx.annotation.Keep;

import com.easy.apt.annotation.sp.Clear;
import com.easy.apt.annotation.sp.Default;
import com.easy.apt.annotation.sp.Preferences;
import com.easy.apt.annotation.sp.Remove;

@Preferences(name = "defaultSp")
@Keep//keep 避免混淆
public interface FrameworkSharePreferences {

    /**
     * 阅读模式--即去广告
     *
     * @return
     */
    @Default("true")
    boolean isReadMode();

    @Clear
    void clear();

    @Remove
    void removeUsername();
}
