package com.easy.framework.base.web;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.easy.framework.base.WebBaseFragment;
import com.easy.framework.even.ChooseFileEvent;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.lang.ref.WeakReference;

public class WebChromeClientBase extends WebChromeClient {
    WeakReference<WebBaseFragment> fragment;

    public WebChromeClientBase(WebBaseFragment fragment) {
        this.fragment = new WeakReference<>(fragment);
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if (fragment.get() != null) {
            fragment.get().setLoadProgress(newProgress);
        }
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        if (fragment.get() != null) {
            fragment.get().receivedWebTitle(title);
        }
    }

    //For Android API >= 21 (5.0 OS)
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, FileChooserParams fileChooserParams) {
        LiveEventBus.get("FileChooser").post(new ChooseFileEvent(null, valueCallback));
        return true;
    }

    // 全屏的时候调用
    @Override
    public void onShowCustomView(View view, CustomViewCallback callback) {

        if (fragment.get() != null) {
            fragment.get().onShowCustomView(view, callback);
        }
        super.onShowCustomView(view, callback);
    }

    // 切换为竖屏的时候调用
    @Override
    public void onHideCustomView() {

        if (fragment.get() != null) {
            fragment.get().onHideCustomView();
        }
        super.onHideCustomView();
    }


}
