package com.easy.framework.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.BuildConfig;
import com.easy.framework.R;
import com.easy.framework.base.web.GestureDetectorListenerImp;
import com.easy.framework.base.web.IWebCallback;
import com.easy.framework.base.web.JsToAndroid;
import com.easy.framework.base.web.VideoListener;
import com.easy.framework.base.web.WebChromeClientBase;
import com.easy.framework.base.web.WebViewClientBase;
import com.easy.framework.base.web.protocol.IProtocolCallback;
import com.easy.framework.base.web.protocol.WebProtocolManager;
import com.easy.framework.databinding.WebVewFragmentBinding;
import com.easy.framework.dialog.WebDownloadDialogFragment;
import com.easy.utils.EmptyUtils;
import com.easy.utils.StringUtils;
import com.easy.utils.ToastUtils;
import com.easy.widget.ScrollWebView;
import com.orhanobut.logger.Logger;


@FragmentInject
public class WebBaseFragment extends BaseFragment<WebBasePresenter, WebVewFragmentBinding> implements WebBaseView, View.OnTouchListener {

    public static final String KEY_RUL = "URL";
    public static final String HTML_DATA = "htmlData";
    public static final String OPEN_GESTURE = "openGesture";
    public static final String SHOW_PROGRESS = "showProgress";
    private String url;
    private String htmlData;
    private boolean openGesture = false;
    /**
     * 进度标识 1:显示进度控件 0:显示加载中转圈圈控件 2:都不显示
     */
    private int showProgressFlag = 1;
    private WebSettings webSettings;
    private ScrollWebView.OnScrollChangeListener onScrollChangeListener;
    private IWebCallback iWebCallback;
    private VideoListener videoListener;

    private IProtocolCallback protocolCallback = (context, uri) -> {
        if (uri != null) {
            ToastUtils.showShort("原生处理协议：" + url);
        }
    };

    GestureDetector detector = new GestureDetector(getContext(), new GestureDetectorListenerImp() {
        @Override
        public void slideToRight() {
            if (viewBind.webView.canGoForward()) {
                viewBind.webView.goForward();
            }
        }

        @Override
        public void slideToLeft() {
            if (viewBind.webView.canGoBack()) {
                viewBind.webView.goBack();
            } else {
                if (iWebCallback != null) {
                    iWebCallback.finish();
                }
            }
        }
    });

    @Override
    public int getLayoutId() {
        return R.layout.web_vew_fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWebCallback) {
            iWebCallback = (IWebCallback) context;
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            url = bundle.getString(KEY_RUL);
            htmlData = bundle.getString(HTML_DATA);
            openGesture = bundle.getBoolean(OPEN_GESTURE, false);
            showProgressFlag = bundle.getInt(SHOW_PROGRESS, 1);
        }
    }

    public void setWebCallback(IWebCallback iWebCallback) {
        this.iWebCallback = iWebCallback;
    }

    public void setVideoListener(VideoListener videoListener) {
        this.videoListener = videoListener;
    }

    @Override
    public void initView(View view) {
        initWebView();
        //scheme 不能包含大写，因为获取到URL时候全是小写的
        WebProtocolManager.getInstall().addScheme("easy", protocolCallback);
        loadUrl();
    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onResume() {
        super.onResume();
        if (webSettings != null) {
            webSettings.setJavaScriptEnabled(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (webSettings != null) {
            webSettings.setJavaScriptEnabled(false);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iWebCallback = null;
    }

    private void initWebView() {
        initWebSetting();
        WebChromeClientBase mWebChromeClientBase = new WebChromeClientBase(this);
        WebViewClientBase mWebViewClientBase = new WebViewClientBase(this);
        viewBind.webView.setWebViewClient(mWebViewClientBase);
        viewBind.webView.setWebChromeClient(mWebChromeClientBase);
        WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG);
        viewBind.webView.setEnabled(true);
        viewBind.webView.setOnLongClickListener(v -> {
            WebView.HitTestResult result = viewBind.webView.getHitTestResult();
            if (result == null) {
                return false;
            }
            if (result.getType() == WebView.HitTestResult.IMAGE_TYPE) {
                presenter.saveImage(result.getExtra());
                return true;
            }
            return false;
        });
        if (iWebCallback != null) {
            JsToAndroid jsToAndroid = iWebCallback.getJsToAndroid();
            if (jsToAndroid != null) {
                viewBind.webView.addJavascriptInterface(jsToAndroid, jsToAndroid.jsName());
            }
        }
        if (openGesture) {
            viewBind.webView.setOnTouchListener(this);
        }
        viewBind.webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {
            Log.d("DownloadListener", "url = " + url
                    + " contentDisposition = " + contentDisposition
                    + " mimeType = " + mimeType
                    + " contentLength = " + contentLength);
            WebDownloadDialogFragment dialogFragment = new WebDownloadDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            bundle.putString("contentDisposition", contentDisposition);
            bundle.putString("mimeType", mimeType);
            bundle.putLong("contentLength", contentLength);
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getChildFragmentManager());
        });

        viewBind.webView.setOnScrollChangeListener((l, t, oldl, oldt) -> {
            if (onScrollChangeListener != null) {
                onScrollChangeListener.onScrollChanged(l, t, oldl, oldt);
            }
        });
    }

    public WebView getWebView() {
        return viewBind.webView;
    }

    private void loadUrl() {
        if (EmptyUtils.isNotEmpty(url)) {
            viewBind.webView.loadUrl(url);
        } else if (EmptyUtils.isNotEmpty(htmlData)) {
            viewBind.webView.loadDataWithBaseURL(null, htmlData, "text/html", "utf-8", null);
        }
    }

    public void updateUrl(String url) {
        //先将webview置空，再进行加载
//        viewBind.webView.loadDataWithBaseURL(null, "","text/html", "utf-8",null);
        this.url = url;
        loadUrl();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void initWebSetting() {
        webSettings = viewBind.webView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        // 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
        // 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可
        webSettings.setJavaScriptEnabled(true);

        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //缩放操作
        //webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
//        webSettings.setBuiltInZoomControls(false); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件

        webSettings.setDomStorageEnabled(true);  // 开启 DOM storage 功能
        webSettings.setAppCacheMaxSize(1024 * 1024 * 50);
        webSettings.setAppCacheEnabled(true);
        Context context = getContext();
        if (context != null) {
            String appCachePath = context.getCacheDir().getAbsolutePath();
            webSettings.setAppCachePath(appCachePath);
        }

        webSettings.setAllowFileAccess(true);    // 可以读取文件缓存
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
        //webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        //不使用缓存:
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setDatabaseEnabled(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        //其他细节操作
        webSettings.setJavaScriptCanOpenWindowsAutomatically(false); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式

        // 特别注意：5.1以上默认禁止了https和http混用，以下方式是开启
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        //设置UserAgent
        String userAgent = webSettings.getUserAgentString();
        webSettings.setUserAgentString(StringUtils.buildString(userAgent, "; app/", getContext().getPackageName(), ";"));
    }

    public void setOnScrollChangeListener(ScrollWebView.OnScrollChangeListener listener) {
        this.onScrollChangeListener = listener;
    }

    public boolean goBack() {
        if (viewBind.webView.canGoBack()) {
            viewBind.webView.goBack();
            return true;
        } else if (iWebCallback != null) {
            iWebCallback.finish();
            return true;
        }
        return false;
    }

    public void reload() {
        viewBind.webView.reload();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (detector != null) {
                return detector.onTouchEvent(motionEvent);
            }
        }
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                view.requestFocusFromTouch();
                break;
        }
        return false;
    }

    /**
     * 加载进度
     *
     * @param newProgress
     */
    public void setLoadProgress(int newProgress) {
        if (showProgressFlag == 1) {
            viewBind.topProgressbar.setProgress(newProgress);
            if (newProgress == 10) {
                viewBind.topProgressbar.setVisibility(View.VISIBLE);
            }
            if (newProgress == 100) {
                viewBind.topProgressbar.setVisibility(View.GONE);
            }
        } else if (showProgressFlag == 0) {
            Logger.i("newProgress======"+newProgress);
            viewBind.ilcProgress.setVisibility(newProgress == 100 ? View.GONE : View.VISIBLE);
        } else {

            if (viewBind.topProgressbar.getVisibility() != View.GONE) {
                viewBind.topProgressbar.setVisibility(View.GONE);
            }
            if (viewBind.ilcProgress.getVisibility() != View.GONE) {
                viewBind.ilcProgress.setVisibility(View.GONE);
            }

            if (iWebCallback != null) {
                iWebCallback.loadProgress(newProgress);
            }

        }

    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {

        fullScreen(true);
        if (videoListener != null) {
            videoListener.onChangeStatus(view, true);
        }
//        viewBind.rlNormalView.setVisibility(View.GONE);
//        viewBind.flVideoContainer.setVisibility(View.VISIBLE);
//        viewBind.flVideoContainer.addView(view);
    }

    // 切换为竖屏的时候调用
    public void onHideCustomView() {

        fullScreen(false);
        if (videoListener != null) {
            videoListener.onChangeStatus(null, false);
        }
//        viewBind.rlNormalView.setVisibility(View.VISIBLE);
//        viewBind.flVideoContainer.setVisibility(View.GONE);
//        viewBind.flVideoContainer.removeAllViews();

    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void fullScreen(boolean landscape) {

        if (landscape) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    /**
     * 接收web里的标题
     *
     * @param title
     */
    public void receivedWebTitle(String title) {
        if (iWebCallback != null) {
            iWebCallback.receivedWebTitle(title);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    public void onPageFinished(WebView webView, String url) {
        if (iWebCallback != null) {
            iWebCallback.loadWebFinish(webView, url);
        }
    }

    public void doUpdateVisitedHistory(String url) {
        if (iWebCallback != null) {
            iWebCallback.showCloseBtn(viewBind.webView.canGoBack());
        }
    }

    public void loadUrl(String url) {
        viewBind.webView.loadUrl(url);
    }

    public void loadJavaScript(String methodArg) {
        viewBind.webView.loadUrl("javascript:" + methodArg);
    }

    public void loadJavaScript(String methodArg, ValueCallback<String> valueCallback) {
        viewBind.webView.evaluateJavascript("javascript:" + methodArg, valueCallback);
    }


}
