package com.easy.app.ui.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.databinding.ChooseTypeDialogBinding;
import com.easy.apt.annotation.FragmentInject;
import com.easy.framework.base.CenterDialogFragment;
import com.easy.utils.DimensUtils;

@FragmentInject
public class ChooseTypeDialogFragment extends CenterDialogFragment<ChooseTypeDialogPresenter, ChooseTypeDialogBinding> implements ChooseTypeDialogView, View.OnClickListener {

    ItemClickListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ItemClickListener) {
            listener = (ItemClickListener) context;
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.choose_type_dialog;
    }


    @Override
    public void initView(View view) {


        String[] items = getString(R.string.type_item).split(",");
        int length = items.length;
        Activity activity = getActivity();
        int dp44 = DimensUtils.dp2px(App.getInst(), 44f);
        for (int i = 0; i < length; i++) {
            String item = items[i];
            AppCompatTextView appCompatTextView = new AppCompatTextView(activity);
            appCompatTextView.setGravity(Gravity.CENTER);
            appCompatTextView.setTextColor(ContextCompat.getColor(App.getInst(), R.color.color_333333));
            appCompatTextView.setTextSize(17f);
            appCompatTextView.setText(item);
            appCompatTextView.setBackgroundResource(R.drawable.btn_selector);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dp44);
            params.gravity = Gravity.CENTER;
            viewBind.llItem.addView(appCompatTextView, params);
            appCompatTextView.setOnClickListener(this);
            if (length - 1 != i) {
                View vLine = new View(activity);
                vLine.setBackgroundResource(R.color.color_e8e8e8);
                params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dp44 / 44 / 2);
                viewBind.llItem.addView(vLine, params);
            }
        }


    }


    @Override
    public void onClick(View v) {
        AppCompatTextView appCompatTextView = (AppCompatTextView) v;
        if (listener != null) {
            listener.itemClick(appCompatTextView.getText().toString());
        }
        dismiss();
    }

    public interface ItemClickListener {

        void itemClick(String typeName);
    }

}
