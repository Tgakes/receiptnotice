package com.easy.app.ui.home;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.HomeBinding;
import com.easy.app.service.ImSocketService;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;
import com.easy.store.bean.Notify;
import com.easy.utils.DateUtils;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;
import java.util.List;

@ActivityInject
@Route(path = RouterManager.HOME_ACTIVITY, name = "首页")
public class HomeActivity extends BaseActivity<HomePresenter, HomeBinding> implements HomeView, View.OnClickListener, ImSocketService.RequestDataListener {

    @Override
    public int getLayoutId() {
        return R.layout.home;
    }

//    @Override
//    public void initStateBar() {
//        super.initStateBar();
//    }

    ImSocketService socketService;

    private boolean serviceRun;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ImSocketService.ImSocketBinder binder = (ImSocketService.ImSocketBinder) service;
            socketService = binder.getService();
            serviceRun = true;
            if (socketService != null) {
                socketService.setHttpCallBack(HomeActivity.this);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

            serviceRun = true;
        }
    };


    public void startService() {

        serviceRun = true;
        Intent intent = new Intent(this, ImSocketService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void stopService() {


        if (serviceRun) {
            unbindService(mConnection);
            serviceRun = false;
        }
    }


    @Override
    public void initView() {
        LiveEventBus.get("notification", StatusBarNotification.class).observe(this, this::notificationReceipt);
        startService();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        presenter.requestAppVersion();
//        String string = Settings.Secure.getString(getContentResolver(),"enabled_notification_listeners");
//        if (!string.contains(NotificationMonitorService.class.getName())) {
//            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
//        }


        Accounts accounts = presenter.accountsDao.get().getAccounts();


        viewBind.acTvLoginName.setText("登录名：" + accounts.getAccount());
        viewBind.acTvMerchantNumber.setText("商户号：" + accounts.getMerchantNumber());
        viewBind.acTvLoginTime.setText("登录时间：" + DateUtils.getDateToString(accounts.getLoginTime(), 4));
        if (!isEnabled()) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
        List<Notify> notifyList = presenter.notifyDao.get().queryAllByUserName(accounts.getAccount());//历史记录
        if (notifyList != null && !notifyList.isEmpty()) {
            int size = notifyList.size()-1;
            for (int i = size; i > 0; i--) {
                Notify notify = notifyList.get(i);
                addView(notify);
            }
        }
        viewBind.acTvExit.setOnClickListener(this);
        viewBind.acTvStartNotice.setOnClickListener(this);
        viewBind.acTvClearCache.setOnClickListener(this);
        presenter.requestPermission(getRxPermissions());
    }

    List<StatusBarNotification> statusBarNotificationList;


    public synchronized void notificationReceipt(StatusBarNotification statusBarNotification) {


        if (statusBarNotification != null) {
            if (statusBarNotificationList == null) {
                statusBarNotificationList = new ArrayList<>();
            }

            Notification notification = statusBarNotification.getNotification();
            Bundle extras = notification.extras;
            // 获取接收消息APP的包名
            String notificationPkg = statusBarNotification.getPackageName();
            // 获取接收消息的抬头
            String notificationTitle = null;
            Object title = extras.getString(Notification.EXTRA_TITLE);
            if (title != null) {
                notificationTitle = title.toString();
            }
            // 获取接收消息的内容
            String notificationText = null;
            Object content = extras.getString(Notification.EXTRA_TEXT);
            if (content != null) {
                notificationText = content.toString();
            }
            if (EmptyUtils.isEmpty(notificationText)) {
                Object object = extras.get(Notification.EXTRA_BIG_TEXT);
                if (object != null) {
                    notificationText = object.toString();
                }
            }
            Log.i("logs","notificationPkg==="+notificationPkg+",notificationTitle==="+notificationTitle+",notificationText==="+notificationText);
            if (EmptyUtils.isEmpty(notificationTitle) || EmptyUtils.isEmpty(notificationText)) {
                return;
            }

            boolean isContain = false;
            if (statusBarNotificationList != null && !statusBarNotificationList.isEmpty()) {
                for (StatusBarNotification barNotification : statusBarNotificationList) {
                    Notification notificationSave = barNotification.getNotification();
                    Bundle extrasSave = notificationSave.extras;
                    // 获取接收消息APP的包名
                    String notificationPkgSave = barNotification.getPackageName();
                    // 获取接收消息的抬头
                    String notificationTitleSave = null;
                    Object titleSave = extrasSave.getString(Notification.EXTRA_TITLE);
                    if (titleSave != null) {
                        notificationTitleSave = titleSave.toString();
                    }
                    // 获取接收消息的内容
                    String notificationTextSave = null;
                    Object contentSave = extrasSave.getString(Notification.EXTRA_TEXT);
                    if (contentSave != null) {
                        notificationTextSave = contentSave.toString();
                    }
                    if (EmptyUtils.isEmpty(notificationTextSave)) {
                        Object objectSave = extrasSave.get(Notification.EXTRA_BIG_TEXT);
                        if (objectSave != null) {
                            notificationTextSave = objectSave.toString();
                        }
                    }

                    if (notificationPkgSave.equals(notificationPkg) && notificationTitleSave.equals(notificationTitle) && notificationTextSave.equals(notificationText)) {
                        isContain = true;
                        break;
                    }
                }
            }

            if (!isContain) {
                statusBarNotificationList.add(statusBarNotification);
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View view = layoutInflater.inflate(R.layout.item_content, null);
                TextView tvItemPhone = view.findViewById(R.id.tvItemPhone);
                TextView tvItemContent = view.findViewById(R.id.tvItemContent);
                tvItemContent.setText("包名：" + notificationPkg + "\n" +
                        "标题：" + notificationTitle + "\n" +
                        "内容：" + notificationText);
                viewBind.llSmsContent.addView(view, 0);
                boolean isExist = presenter.uploadNotice(notificationPkg,notificationTitle, notificationText,tvItemPhone);
                if (!isExist) {
                    Notify notify = new Notify();
                    notify.setContent(notificationText);
                    notify.setMsg("不存在指定类别");
                    notify.setPackageName(notificationPkg);
                    notify.setTitle(notificationTitle);
                    notify.setUpload(false);
                    notify.setUserName(presenter.accountsDao.get().getUserName());
                    presenter.notifyDao.get().add(notify);
                    tvItemPhone.setTextColor(getResources().getColor(R.color.color_777777));
                    tvItemContent.setTextColor(getResources().getColor(R.color.color_777777));
                    tvItemPhone.setText("不存在指定类别");
                }
            }

        }
    }


    public void addView(Notify notify) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_content, null);
        TextView tvItemPhone = view.findViewById(R.id.tvItemPhone);
        TextView tvItemContent = view.findViewById(R.id.tvItemContent);
        tvItemPhone.setText(notify.getMsg());
        tvItemContent.setText("包名：" + notify.getPackageName() + "\n" +
                "标题：" + notify.getTitle() + "\n" +
                "内容：" + notify.getContent());
        if (!notify.isUpload()) {
            tvItemPhone.setTextColor(getResources().getColor(R.color.color_777777));
            tvItemContent.setTextColor(getResources().getColor(R.color.color_777777));
        }
        viewBind.llSmsContent.addView(view);



    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.acTvClearCache:
                showLoading("请稍后...");
                v.postDelayed(() -> {
                    if (statusBarNotificationList != null) {
                        statusBarNotificationList.clear();
                    }
                    presenter.notifyDao.get().deleteAll(presenter.accountsDao.get().getUserName());
                    viewBind.llSmsContent.removeAllViews();
                    hideLoading();
                    ToastUtils.showShort("已清除缓存");
                },1500);
                break;
            case R.id.acTvStartNotice:

                if (!isEnabled()) {
                    startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                } else {
                    ToastUtils.showShort("监控器开关已打开");
                }

                break;
            case R.id.acTvExit:
                presenter.accountsDao.get().delete();
                RouterManager.goLoginActivity(this);
                break;
        }
    }

    // 判断是否打开了通知监听权限
    private boolean isEnabled() {
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        if (!EmptyUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;

    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (e != null) {
            ToastUtils.showShort(",error：" + e.getMessage());
            finish();
        } else {
            if (granted != null && granted) {
//                startService(new Intent(this, FirebaseMsgService.class));

            } else {
                ToastUtils.showShort("拒绝权限申请将无法接收消息");

            }
        }
    }

    @Override
    public void noticeCallback(Response<String> response, TextView tvItemStatus) {
        if (response.isSuccess()) {
            tvItemStatus.setText(response.getMsg() + ",\n订单号为：" + response.getOrderid());

        } else {
            tvItemStatus.setText(response.getMsg());
        }
    }

    @Override
    public void heartbeatCallback(Response response) {

        if (response.isSuccess()) {
            viewBind.ivHeartbeatStatus.setText("心跳包监测正常，最新监测时间为：\n" + DateUtils.getDateToString(Long.valueOf(response.getAddtime()) * 1000, 4));
//            viewBind.ivHeartbeatStatus.setText();
        } else {
            viewBind.ivHeartbeatStatus.setText("监测心跳包返回异常：" + response.getMsg());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService();
        if (statusBarNotificationList != null) {
            statusBarNotificationList.clear();
            statusBarNotificationList = null;
        }
    }

    @Override
    public void onRequest() {
        runOnUiThread(() -> {
            viewBind.ivHeartbeatStatus.setText("正在监测心跳包...");
            presenter.requestHeartbeat();
        });

    }
}
