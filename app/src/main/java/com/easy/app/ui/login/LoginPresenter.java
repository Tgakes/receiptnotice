package com.easy.app.ui.login;

import androidx.lifecycle.Lifecycle;

import com.easy.app.base.AppPresenter;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;
import com.easy.utils.EmptyUtils;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter extends AppPresenter<LoginView> {

    @Inject
    public LoginPresenter() {

    }


    public void requestSms(String telNum) {

        Map<String, Object> parameter = new HashMap<>();
        parameter.put("actsms", 3);//1注册 2转账 3登录 4兑换 5提现 6修改资料
        parameter.put("number", telNum);//接收方手机号
        RxHttp.post(UrlConstant.SMS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.smsCallback(result);
                        },
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.smsCallback(response);
                        });
    }


    public void requestLogin(String merchantNumber,String account, String pwd) {

        Map<String, Object> parameter = new HashMap<>();
        parameter.put("username", account);
        parameter.put("password", pwd);
        parameter.put("mch", merchantNumber);
        parameter.put("bussion", "login");
        RxHttp.post(UrlConstant.HOST_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(Accounts.class)
                .map(loginResponse -> {
                    Accounts accounts;
                    if (loginResponse.isSuccess() && (accounts = loginResponse.getResultObj()) != null && EmptyUtils.isNotEmpty(accounts.getTitle())) {
//                        accounts.setPhone(account);
                        accounts.setDomain(UrlConstant.HOST_ADDRESS);
                        accounts.setAccount(account);
                        accounts.setMerchantNumber(merchantNumber);
                        accounts.setLoginTime(System.currentTimeMillis());
                        accountsDao.get().add(accounts);
                    }

                    return loginResponse;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.loginCallback(result);
                        },
                        throwable -> {
                            Response<Accounts> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.loginCallback(response);
                        });
    }

}
