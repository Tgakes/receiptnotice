package com.easy.app.ui.welcome;

import android.Manifest;

import androidx.lifecycle.Lifecycle;

import com.easy.app.base.AppPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

public class WelcomePresenter extends AppPresenter<WelcomeView> {
    @Inject
    public WelcomePresenter() {

    }


    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WAKE_LOCK
        )
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }
}