package com.easy.app.ui.login;


import com.easy.framework.base.BaseView;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;

public interface LoginView extends BaseView {


    void loginCallback(Response<Accounts> response);

    void smsCallback(Response response);
}
