package com.easy.app.ui.home;

import android.Manifest;
import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.text.SpannableString;
import android.util.Log;
import android.widget.TextView;

import androidx.lifecycle.Lifecycle;

import com.alibaba.fastjson.JSON;
import com.easy.app.base.AppPresenter;
import com.easy.net.RxHttp;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;
import com.easy.store.bean.Notify;
import com.easy.store.dao.AccountsDao;
import com.easy.store.dao.NotifyDao;
import com.easy.utils.EmptyUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;

import dagger.Lazy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter extends AppPresenter<HomeView> {

    @Inject
    public Lazy<NotifyDao> notifyDao;


    @Inject
    public HomePresenter() {
    }


    /**
     * 请求版本更新
     */
    public void requestAppVersion() {

      /*  RxHttp.get(UrlConstant.VERSION_UPDATE)
                .request(AppVersion.class)
                .map(objectResponse -> {
                    objectResponse.setResultObj(JSON.parseObject(objectResponse.getOriginalData(), AppVersion.class));
                    if (objectResponse.getResultObj() != null) {
                        objectResponse.setCode(Response.SUCCESS_STATE);
                    }
                    return objectResponse;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> mvpView.appVersionCallback(result),
                        throwable -> {
                            Response<AppVersion> response = new Response<>();
                            response.setCode(Response.ERROR_STATE);
                            if (throwable != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.appVersionCallback(response);
                        });*/
    }


    public boolean uploadNotice(String notificationPkg,String notificationTitle,String notificationText, TextView tvItemStatus) {



//        Bundle extras = notification.extras;
        //获取bundle中所有key的值
      /*  Set<String> getKey = extras.keySet();
        for (String key : getKey) {
            Log.i("logs", "key====" + key + ",name====" + extras.get(key));
        }*/



//        Log.i("logs", "onNotificationPosted notificationPkg===" + notificationPkg + ",notificationTitle===" + notificationTitle + ",notificationText===" + notificationText);
        Accounts accounts = accountsDao.get().getAccounts();
        if (accounts == null || EmptyUtils.isEmpty(accounts.getTitle())) {
            return false;
        }

        List<String> packageNames = accountsDao.get().getPackageNames();
        boolean isPackage = false;
        for (String packageName : packageNames) {
            if (packageName.equals(notificationPkg)) {
                isPackage = true;
                break;
            }
        }

        if (!isPackage) {
            return false;
        }


        List<String> titles = accountsDao.get().getTitles();

        boolean isExist = false;
        if (titles != null && !titles.isEmpty()) {
            for (String str : titles) {
                if (notificationTitle.contains(str) || notificationText.contains(str)) {
                    isExist = true;
                    break;
                }
            }
        }

        if (!isExist) {
            return false;
        }
       /* String domain = accounts.getDomain();
        if (EmptyUtils.isEmpty(domain)) {
            return;
        }*/
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("title", notificationTitle);
        parameter.put("strmake", notificationText);
        parameter.put("package", notificationPkg);
        parameter.put("orderid", getOrderId());
        parameter.put("token", accounts.getToken());
        parameter.put("uid", accounts.getId());
        parameter.put("bussion", "notify");

        RxHttp.post(UrlConstant.HOST_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .map(response -> {
                    Notify notify = new Notify();
                    notify.setContent(notificationText);
                    notify.setPackageName(notificationPkg);
                    notify.setTitle(notificationTitle);
                    notify.setUpload(false);
                    notify.setUserName(accountsDao.get().getUserName());
                    if (response.isSuccess()) {
                        notify.setMsg(response.getMsg() + ",\n订单号为：" + response.getOrderid());
                    }else {
                        notify.setMsg(response.getMsg());
                    }
                    notifyDao.get().add(notify);
                    return response;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {

                            mvpView.noticeCallback(result, tvItemStatus);
                        },
                        throwable -> {
                            Response<String> response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.noticeCallback(response, tvItemStatus);
                            Notify notify = new Notify();
                            notify.setContent(notificationText);
                            notify.setMsg(throwable.getMessage());
                            notify.setPackageName(notificationPkg);
                            notify.setTitle(notificationTitle);
                            notify.setUpload(false);
                            notify.setUserName(accountsDao.get().getUserName());
                            notifyDao.get().add(notify);
                        });

        return true;
    }

    public static String getOrderId() {


        return getRandomString(4) + System.currentTimeMillis();
    }


    //length用户要求产生字符串的长度
    public static String getRandomString(int length) {
        String str = "0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(10);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


    public void requestHeartbeat() {


        Accounts accounts = accountsDao.get().getAccounts();
        if (accounts == null) {
            return;
        }
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("token", accounts.getToken());
        parameter.put("uid", accounts.getId());
        parameter.put("bussion", "heartbeat");
        RxHttp.post(UrlConstant.HOST_ADDRESS)
                .addHeader(getHeaderMap())
                .setBodyString(getEncryptData(parameter))
                .request(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(result -> {
                            mvpView.heartbeatCallback(result);
                        },
                        throwable -> {
                            Response response = new Response<>();
                            if (throwable != null && throwable.getMessage() != null) {
                                response.setMsg(throwable.getMessage());
                            }
                            mvpView.heartbeatCallback(response);
                        });
    }


    /**
     * 请求权限
     *
     * @param permissions
     */
    public void requestPermission(RxPermissions permissions) {
        permissions.request(
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WAKE_LOCK
        )
                .as(getAutoDispose(Lifecycle.Event.ON_DESTROY))
                .subscribe(granted -> mvpView.permissionCallback(granted, null),
                        throwable -> mvpView.permissionCallback(null, throwable));
    }
}