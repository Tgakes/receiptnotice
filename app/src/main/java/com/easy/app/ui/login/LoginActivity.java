package com.easy.app.ui.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.LoginBinding;

import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.framework.manager.activity.ActivityManager;
import com.easy.net.UrlConstant;
import com.easy.net.beans.Response;

import com.easy.store.bean.Accounts;
import com.easy.utils.EmptyUtils;
import com.easy.utils.ToastUtils;

import com.easy.widget.SMSCountDownTimer;
import com.easy.widget.TitleView;


@ActivityInject
@Route(path = RouterManager.LOGIN_ACTIVITY, name = "登录页面")
public class LoginActivity extends BaseActivity<LoginPresenter, LoginBinding> implements LoginView, View.OnClickListener, TextWatcher {


    @Override
    public int getLayoutId() {
        return R.layout.login;
    }

    @Override
    public void initView() {
        ActivityManager.getInstance().finishOtherActivity();
        TitleView titleView =addTitleView(getString(R.string.login));
        if (titleView != null) {
            titleView.hideBackBtn();
        }


        viewBind.domainEdit.setText(UrlConstant.HOST_ADDRESS);
        PasswordHelper.bindPasswordEye(viewBind.passwordEdit, findViewById(R.id.tbEye));
        viewBind.sendAgainBtn.setOnClickListener(this);
        viewBind.loginBtn.setOnClickListener(this);
        viewBind.forgetPasswordBtn.setOnClickListener(this);
        viewBind.registerAccountBtn.setOnClickListener(this);
        viewBind.phoneNumberEdit.addTextChangedListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.send_again_btn:


                String phone1 = viewBind.phoneNumberEdit.getText().toString();
                if (EmptyUtils.isEmpty(phone1)) {
                    ToastUtils.showShort(getString(R.string.user_name_hint));
                    return;
                }


                viewBind.sendAgainBtn.setEnabled(false);
                viewBind.sendAgainBtn.setText(getString(R.string.getting));
                presenter.requestSms(phone1);
                break;*/
            case R.id.login_btn:
               /* String domain = viewBind.domainEdit.getText().toString();
                if (EmptyUtils.isEmpty(domain)) {
                    ToastUtils.showShort(getString(R.string.input_domain));
                    return;
                }
                if (!domain.endsWith("/")) {
                    domain += "/";
                }*/


                String merchantNumber = viewBind.merchantNumberEdit.getText().toString();
                if (EmptyUtils.isEmpty(merchantNumber)) {
                    ToastUtils.showShort(getString(R.string.input_merchant_number));
                    return;
                }

                String phone = viewBind.phoneNumberEdit.getText().toString();
                if (EmptyUtils.isEmpty(phone)) {
                    ToastUtils.showShort(getString(R.string.user_name_hint));
                    return;
                }


                String pwd = viewBind.passwordEdit.getText().toString();
                if (EmptyUtils.isEmpty(pwd)) {
                    ToastUtils.showShort(getString(R.string.please_input_password));
                    return;
                }
//                String code = "";
//                if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
//                    code = viewBind.authCodeEdit.getText().toString();
//                    if (EmptyUtils.isEmpty(code)) {
//                        ToastUtils.showShort(getString(R.string.input_message_code));
//                        return;
//                    }
//                }
                viewBind.phoneNumberEdit.setTag(phone);
                showLoading();
                presenter.requestLogin(merchantNumber, phone, pwd);
                break;
         /*   case R.id.forget_password_btn:
                RouterManager.goFindPwdActivity();
                break;
            case R.id.register_account_btn:
                RouterManager.goRegisterActivity();
                break;*/
        }
    }

    @Override
    public void loginCallback(Response<Accounts> response) {

        Log.i("logs","loginCallback response===="+response);
        hideLoading();
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            if (response.getResultObj() != null && EmptyUtils.isNotEmpty(response.getResultObj().getTitle())) {
                RouterManager.goHomeActivity(this);
            }else {
                ToastUtils.showShort("抓取类别为空");
            }

        }
    }

    @Override
    public void smsCallback(Response response) {
        ToastUtils.showShort(response.getMsg());
        if (response.isSuccess()) {
            new SMSCountDownTimer(viewBind.sendAgainBtn, 60000, 1000, getString(R.string.get_msg_code));
        } else {
            viewBind.sendAgainBtn.setEnabled(true);
            viewBind.sendAgainBtn.setText(getString(R.string.get_msg_code));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        String needShowPhone = (String) viewBind.phoneNumberEdit.getTag();//需要验证码的手机号
        if (EmptyUtils.isEmpty(needShowPhone)) {//
            if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {//输入框为空隐藏验证码
                viewBind.authCodeLl.setVisibility(View.GONE);
            }
        } else {
            if (needShowPhone.equals(s.toString())) {//显示验证码
                if (viewBind.authCodeLl.getVisibility() == View.GONE) {
                    viewBind.authCodeLl.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewBind.authCodeLl.getVisibility() == View.VISIBLE) {
                    viewBind.authCodeLl.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
