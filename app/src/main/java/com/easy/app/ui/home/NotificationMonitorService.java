package com.easy.app.ui.home;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.jeremyliao.liveeventbus.LiveEventBus;

public class NotificationMonitorService extends NotificationListenerService {

    // 在收到消息时触发
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        LiveEventBus.get("notification", StatusBarNotification.class).post(sbn);
    }

    // 在删除消息时触发
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {

        Notification notification = sbn.getNotification();
        Bundle extras = notification.extras;
        // 获取接收消息APP的包名
        String notificationPkg = sbn.getPackageName();
        // 获取接收消息的抬头
        String notificationTitle = extras.getString(Notification.EXTRA_TITLE);
        // 获取接收消息的内容
        String notificationText = extras.getString(Notification.EXTRA_TEXT);
        Log.i("logs", "onNotificationRemoved notificationPkg===" + notificationPkg + ",notificationTitle===" + notificationTitle + ",notificationText===" + notificationText + ",tickerText===" + notification.tickerText);

    }
}
