package com.easy.app.ui.welcome;

import android.content.Intent;

import com.easy.app.R;
import com.easy.app.base.App;
import com.easy.app.base.RouterManager;
import com.easy.app.databinding.WelcomeBinding;
import com.easy.apt.annotation.ActivityInject;
import com.easy.framework.base.BaseActivity;
import com.easy.utils.ToastUtils;


@ActivityInject
public class WelcomeActivity extends BaseActivity<WelcomePresenter, WelcomeBinding> implements WelcomeView {

    @Override
    public int getLayoutId() {
        return R.layout.welcome;
    }

    @Override
    public void initView() {
        //首次启动 Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT 为 0，再次点击图标启动时就不为零了
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        presenter.requestPermission(getRxPermissions());


    }

    @Override
    public void permissionCallback(Boolean granted, Throwable e) {
        if (e != null) {

        }else {
            if (granted != null && granted) {
                viewBind.container.postDelayed(() -> {
                    if (presenter.accountsDao.get().isLogin()) {
                        RouterManager.goHomeActivity(this);
                    } else {
                        RouterManager.goLoginActivity(this);
                    }
//            WelcomeActivity.this.finish();
                }, 3000);
            }else {
                ToastUtils.showShort("拒绝权限申请将无法接收消息");
            }
        }
    }
}
