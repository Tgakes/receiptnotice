package com.easy.app.ui.home;

import android.widget.TextView;

import com.easy.framework.base.BaseView;
import com.easy.framework.bean.AppVersion;
import com.easy.net.beans.Response;
import com.easy.store.bean.Accounts;

public interface HomeView extends BaseView {

//    void appVersionCallback(Response<AppVersion> response);

    void permissionCallback(Boolean granted, Throwable e);


    void noticeCallback(Response<String> response, TextView tvStatus);


    void heartbeatCallback(Response response);
}
