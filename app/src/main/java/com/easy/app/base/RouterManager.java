package com.easy.app.base;

import android.app.Activity;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RouterManager {
    public static final String ROUTER_HOME = "/app/";
    public static final String WEB_ACTIVITY = ROUTER_HOME + "WebActivity";
    public static final String HOME_ACTIVITY = ROUTER_HOME + "HomeActivity";
    public static final String CAMERA = ROUTER_HOME + "CameraActivity";
    public static final String LOCAL_FILE_ACTIVITY = ROUTER_HOME + "LocalFileActivity";
    public static final String CHOOSE_CAPTURE = ROUTER_HOME + "ChooseCaptureActivity";
    public static final String READ_SMS = ROUTER_HOME + "ReadSmsActivity";
    public static final String LOGIN_ACTIVITY = ROUTER_HOME + "LoginActivity";

    public static void goHomeActivity(Activity activity) {
        ARouter.getInstance().build(HOME_ACTIVITY)
                .navigation(activity, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {
                        if (!activity.isFinishing()) {
                            activity.finish();
                        }
                    }
                });
    }

    public static void goLoginActivity(Activity activity) {
        ARouter.getInstance().build(LOGIN_ACTIVITY)
                .navigation(activity, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {

                        if (!activity.isFinishing()) {
                            activity.finish();
                        }
                    }
                });
    }

    public static void goReadSmsActivity(String tel1,String tel2) {
        ARouter.getInstance().build(READ_SMS)
                .withString("tel1", tel1).withString("tel2", tel2).navigation();
    }

    public static void goCameraActivity(String savePath, String typeName,String tel) {
        ARouter.getInstance().build(CAMERA).withString("savePath", savePath)
                .withString("typeName", typeName).withString("tel", tel).navigation();
    }

    public static void goChooseCaptureActivity(String tel, String typeName) {
        ARouter.getInstance().build(CHOOSE_CAPTURE).withString("tel", tel)
                .withString("typeName", typeName).navigation();
    }

    public static void goLocalFileActivity(int type) {
        ARouter.getInstance().build(LOCAL_FILE_ACTIVITY).withInt("type", type).navigation();
    }


    public static void goWebActivity(@Nonnull String url, @Nullable String title) {
        ARouter.getInstance().build(WEB_ACTIVITY)
                .withString("url", url)
                .withString("title", title).navigation();
    }


}
