package com.easy.app.base;

import com.alibaba.fastjson.JSON;
import com.easy.framework.base.BasePresenter;
import com.easy.framework.base.BaseView;
import com.easy.store.dao.AccountsDao;
import com.easy.utils.BinaryStringConvertUtils;
import com.easy.utils.DeviceUtils;
import com.easy.utils.RsaUtils;
import com.easy.utils.Sha1Utils;
import com.easy.utils.ToastUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import dagger.Lazy;

public class AppPresenter<V extends BaseView> extends BasePresenter<V> {


    @Inject
    public Lazy<AccountsDao> accountsDao;

    public static final String SECRET_KEY =  "Shangwu*!(2515Dev";

    /**
     * 获取加密字符串
     * @param parameter
     * @return
     */
    public String getEncryptData(Map<String, Object> parameter){

        try {
            String data = RsaUtils.encryptRSA(URLEncoder.encode(JSON.toJSONString(parameter), "UTF-8"));
            return BinaryStringConvertUtils.toBinaryString(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ToastUtils.showShort("URLEncoder exception=========");
        return null;
    }


    public Map<String, Object> getHeaderMap() {

        Map<String, Object> header = new HashMap<>();
        String nonce = getRandomString(6);
        long timestamp = System.currentTimeMillis();
        header.put("nonce",nonce);
        header.put("timestamp",String.valueOf(timestamp));
        header.put("sign", Sha1Utils.encryptToSHA(nonce + "|" + SECRET_KEY + "|" + timestamp));
        header.put("version","1.0");
        header.put("platform","2");
        header.put("platid", DeviceUtils.getImei(App.getInst()));
        return header;

    }

    //length用户要求产生字符串的长度
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}
