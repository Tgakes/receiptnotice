package com.easy.app.base;

import com.easy.framework.base.BaseApplication;
import com.easy.net.UrlConstant;
import com.easy.net.retrofit.RetrofitConfig;

import java.util.HashMap;
import java.util.Map;

public class App extends BaseApplication {


    @Override
    protected void initBaseConfig(RetrofitConfig.Builder builder) {
        Map<String, String> hostMap = new HashMap<>();

        builder.baseUrl(UrlConstant.HOST_ADDRESS)
                .supportCookies(true)
                .supportHostGroup(hostMap)
                .forceCache(true, 600);
    }

    @Override
    public void initOnThread() {

    }

    @Override
    public void initOnMainThread() {

    }


}
