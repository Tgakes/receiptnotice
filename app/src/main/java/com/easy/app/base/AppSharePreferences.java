package com.easy.app.base;


import androidx.annotation.Keep;

import com.easy.apt.annotation.sp.Clear;
import com.easy.apt.annotation.sp.Default;
import com.easy.apt.annotation.sp.Expired;
import com.easy.apt.annotation.sp.Preferences;
import com.easy.apt.annotation.sp.Remove;

@Preferences(name = "videoSp")
@Keep//keep 避免混淆
public interface AppSharePreferences {



    @Default("")
    String getFaceUpload();

    void setFaceUpload(String faceUpload);


    @Default("")
    String getNodHeadUpload();

    void setNodHeadUpload(String nodHeadUpload);


    @Default("")
    String getShakeHeadLeftUpload();

    void setShakeHeadLeftUpload(String shakeHeadLeftUpload);


    @Default("")
    String getShakeHeadRightUpload();

    void setShakeHeadRightUpload(String shakeHeadRightUpload);



    @Default("")
    String getOpenMouthUpload();

    void setOpenMouthUpload(String openMouthUpload);


    @Default("")
    String getBlinkUpload();

    void setBlinkUpload(String blinkUpload);
}
