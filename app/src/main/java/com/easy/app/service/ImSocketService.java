package com.easy.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.easy.app.tool.RxTimerTool;


/**
 * im
 */
public class ImSocketService extends Service {




    private final IBinder mBinder = new ImSocketBinder();




    @Override
    public void onCreate() {
        super.onCreate();
//        注册锁屏广播监听器
//        screenReceiverUtils = new ScreenReceiverUtils(this);
//        screenReceiverUtils.setScreenReceiverListener(mScreenListener);
    }

    RequestDataListener listener;

    public void setHttpCallBack(RequestDataListener listener) {
        this.listener = listener;
        rxTimerTool = RxTimerTool.getInstance();
        if (this.listener != null) {
            this.listener.onRequest();
            //30秒检测一次
            rxTimerTool.intervalThread(30000, number -> this.listener.onRequest());
        }
    }




    RxTimerTool rxTimerTool;



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        startRunTimer();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (rxTimerTool != null)
            rxTimerTool.cancel();
//        screenReceiverUtils.stopScreenReceiverListener();
    }




    public class ImSocketBinder extends Binder {


        public ImSocketService getService() {

            return ImSocketService.this;
        }
    }


    public interface RequestDataListener {

        void onRequest();


    }

}
