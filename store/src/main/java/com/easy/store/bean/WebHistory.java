package com.easy.store.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;

@Entity
public class WebHistory implements Comparable<WebHistory> {
    @Id
    private long id;//数据库ID
    private String url;//浏览网页地址
    private String title;
    private long time;
    private String timeStr;
    @Transient
    private String showTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String isShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }


    public String getShowTime() {
        return showTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int compareTo(WebHistory o) {
        return (int) (o.time - time);
    }

    @Override
    public String toString() {
        return "WebHistory{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", time=" + time +
                ", timeStr='" + timeStr + '\'' +
                ", showTime='" + showTime + '\'' +
                '}';
    }
}
