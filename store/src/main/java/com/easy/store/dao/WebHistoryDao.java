package com.easy.store.dao;

import com.easy.store.bean.WebHistory;
import com.easy.store.bean.WebHistory_;

import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;

public class WebHistoryDao extends BaseDao {
    Box<WebHistory> historyBox;
    private ObjectBoxLiveData<WebHistory> historyLiveData;

    @Inject
    public WebHistoryDao() {
        if (boxStore != null) {
            historyBox = boxStore.boxFor(WebHistory.class);
        }
    }

    public ObjectBoxLiveData<WebHistory> getHistoryLiveData() {
        if (historyLiveData == null) {
            historyLiveData = new ObjectBoxLiveData<>(historyBox.query().build());
        }
        return historyLiveData;
    }

    public boolean add(WebHistory history) {
        if (isHistory(history.getUrl())) {
            return false;
        } else {
            historyBox.put(history);
            return true;
        }
    }

    public List<WebHistory> queryAll() {
        return historyBox.query().build().find();
    }

    public boolean isHistory(String url) {
        WebHistory webHistory = historyBox.query().equal(WebHistory_.url, url).build().findFirst();
        return webHistory != null;
    }

    public boolean deleteHistory(String url) {
        WebHistory webHistory = historyBox.query().equal(WebHistory_.url, url).build().findFirst();
        if (webHistory != null) {
            historyBox.remove(webHistory);
            return true;
        }
        return false;
    }

    public boolean deleteHistoryAll() {
        historyBox.removeAll();
        return true;
    }
}
