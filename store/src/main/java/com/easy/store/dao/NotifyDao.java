package com.easy.store.dao;

import android.text.TextUtils;

import com.easy.store.bean.Notify;
import com.easy.store.bean.Notify_;


import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;

public class NotifyDao extends BaseDao {

    Box<Notify> historyBox;

    private ObjectBoxLiveData<Notify> historyLiveData;

    @Inject
    public NotifyDao() {
        if (boxStore != null) {
            historyBox = boxStore.boxFor(Notify.class);
        }
    }

    public ObjectBoxLiveData<Notify> getHistoryLiveData() {
        if (historyLiveData == null) {
            historyLiveData = new ObjectBoxLiveData<>(historyBox.query().build());
        }
        return historyLiveData;
    }

    public void add(Notify history) {
        historyBox.put(history);
    }

    public List<Notify> queryAllByUserName(String userName) {

        return TextUtils.isEmpty(userName) ? null : historyBox.query().equal(Notify_.userName, userName).build().find();
    }


    public void deleteAll(String userName) {
        List<Notify> notifyList = queryAllByUserName(userName);
        if (notifyList != null && !notifyList.isEmpty()) {
            historyBox.remove(notifyList);
        }
    }
}
