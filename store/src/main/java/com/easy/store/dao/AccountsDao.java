package com.easy.store.dao;

import com.alibaba.fastjson.JSON;
import com.easy.store.bean.Accounts;

import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;

public class AccountsDao extends BaseDao {

    Box<Accounts> accountsBox;
    private ObjectBoxLiveData<Accounts> accountsLiveData;

    Accounts accounts;

    @Inject
    public AccountsDao() {
        if (boxStore != null) {
            accountsBox = boxStore.boxFor(Accounts.class);
        }
        accounts = getAccounts();
    }

    public ObjectBoxLiveData<Accounts> getAccountsLiveData() {
        if (accountsLiveData == null) {
            accountsLiveData = new ObjectBoxLiveData<>(accountsBox.query().build());
        }
        return accountsLiveData;
    }

    public void add(Accounts accounts) {
        accountsBox.removeAll();
        accountsBox.put(accounts);
    }

    public void delete() {
        accountsBox.removeAll();
    }

    public void update(Accounts accounts) {
        accountsBox.put(accounts);
    }

    public Accounts getAccounts() {

        return accounts == null ? queryAccounts() : accounts;
    }

    public String getUserName() {
        Accounts accounts =getAccounts();
        return accounts != null ? accounts.getAccount() : "";
    }

    public Accounts queryAccounts() {

        return accountsBox.query().build().findFirst();
    }



    List<String> packageNames;

    public List<String> getPackageNames() {
        return packageNames == null ? JSON.parseArray(accounts.getPackagename(), String.class) : null;
    }


    List<String> titles;

    public List<String> getTitles() {
        return titles == null ? JSON.parseArray(accounts.getTitle(), String.class) : null;
    }





    public boolean isLogin() {
        return getAccounts() != null;
    }
}
