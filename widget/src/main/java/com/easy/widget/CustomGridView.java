package com.easy.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;


/**
 * 去阴影部分
 *
 * @author gakes
 */

public class CustomGridView extends GridView {

    public CustomGridView(Context context) {
        super(context);
    }

    boolean isSlide;

    public CustomGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (Build.VERSION.SDK_INT >= 9) {
            this.setOverScrollMode(View.OVER_SCROLL_NEVER);
        }
        setCustomAttributes(context, attrs);
    }

    public CustomGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomAttributes(context, attrs);

    }

    private void setCustomAttributes(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SwitchSlide);
        isSlide = array.getBoolean(R.styleable.SwitchSlide_isSlide, true);
        array.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, isSlide ? heightMeasureSpec : View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST));
    }


    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!isSlide && ev.getAction() == MotionEvent.ACTION_MOVE) {
            return true; // 禁止GridView滑动
        }
        return super.dispatchTouchEvent(ev);
    }


    public void setSlide(boolean slide) {
        isSlide = slide;
    }
}
